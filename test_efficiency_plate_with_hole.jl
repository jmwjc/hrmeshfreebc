

using ApproxOperator,BenchmarkTools

ndiv = 24
elements, nodes = importmsh("./msh/plate_with_hole_"*string(ndiv)*".msh")
nₚ = length(nodes[:x])
nₑ = length(elements["Ω"])

type = (SNode,:Cubic2D,:□,:CubicSpline)
s = 3.5*1.6/ndiv*ones(nₚ)
sp = RegularGrid(nodes[:x],nodes[:y],nodes[:z],n = 2,γ = 5)
elements["Ω"] = ReproducingKernel{type...,:Tri3}(elements["Ω"],sp)
elements["Ω̃"] = ReproducingKernel{type...,:Tri3}(elements["Ω"])
elements["Γˡ"] = Element{:Seg2}([elements["Γᵍ₁"];elements["Γᵍ₂"]],renumbering=true)
elements["Γ̃ᵍ₁"] = ReproducingKernel{type...,:Seg2}(elements["Γᵍ₁"])
elements["Γ̃ᵍ₂"] = ReproducingKernel{type...,:Seg2}(elements["Γᵍ₂"])
elements["Γᵍ₁"] = ReproducingKernel{type...,:Seg2}(elements["Γᵍ₁"],sp)
elements["Γᵍ₂"] = ReproducingKernel{type...,:Seg2}(elements["Γᵍ₂"],sp)
elements["Ω∩Γᵍ₁"] = elements["Ω"]∩elements["Γᵍ₁"]
elements["Ω∩Γᵍ₂"] = elements["Ω"]∩elements["Γᵍ₂"]
nₗ = getnₚ(elements["Γˡ"])

# set𝓖!(elements["Ω"],:TriGI13,:∂1,:∂x,:∂y,:∂z)
# set𝓖!(elements["Γᵍ"],:SegGI3,:∂1)
# set𝓖!(elements["Γ̃ᵍ"],:SegGI3,:∂1,:∂x,:∂y,:∂z)
set𝓖!(elements["Ω"],:TriRK13,:∂1)
set𝓖!(elements["Ω̃"],:TriGI6,:∂1,:∂x,:∂y,:∂z)
set𝓖!(elements["Γᵍ₁"],:SegRK5,:∂1,:∂x,:∂y,:∂z,:∂̄x,:∂̄y)
set𝓖!(elements["Γᵍ₂"],:SegRK5,:∂1,:∂x,:∂y,:∂z,:∂̄x,:∂̄y)
set𝓖!(elements["Γ̃ᵍ₁"],:SegRK5,:∂1,:∂x,:∂y,:∂z)
set𝓖!(elements["Γ̃ᵍ₂"],:SegRK5,:∂1,:∂x,:∂y,:∂z)


E = 3E6;ν = 0.3;T = 1000;a = 1.0;

r(x,y) = (x^2+y^2)^0.5
θ(x,y) = atan(y/x)
σ₁₁(x,y) = T - T*a^2/r(x,y)^2*(3/2*cos(2*θ(x,y))+cos(4*θ(x,y))) + T*3*a^4/2/r(x,y)^4*cos(4*θ(x,y))
σ₂₂(x,y) = -T*a^2/r(x,y)^2*(1/2*cos(2*θ(x,y))-cos(4*θ(x,y))) - T*3*a^4/2/r(x,y)^4*cos(4*θ(x,y))
σ₁₂(x,y) = -T*a^2/r(x,y)^2*(1/2*sin(2*θ(x,y))+sin(4*θ(x,y))) + T*3*a^4/2/r(x,y)^4*sin(4*θ(x,y))
prescribe!(elements["Γᵍ₁"],:n₂₂,(x,y,z)->1.0)
prescribe!(elements["Γᵍ₂"],:n₁₁,(x,y,z)->1.0)
prescribe!(elements["Γ̃ᵍ₁"],:n₂₂,(x,y,z)->1.0)
prescribe!(elements["Γ̃ᵍ₂"],:n₁₁,(x,y,z)->1.0)

push!(nodes,:s₁=>s,:s₂=>s,:s₃=>s)
@btime set𝝭!($elements["Ω"])
@btime set∇̃𝝭!($elements["Ω̃"],$elements["Ω"])
@btime set𝝭!($elements["Γᵍ₁"])
@btime set𝝭!($elements["Γᵍ₂"])
@btime set∇𝝭!($elements["Γ̃ᵍ₁"])
@btime set∇𝝭!($elements["Γ̃ᵍ₂"])

coefficient = (:E=>E,:ν=>ν,:α=>1e7*E)
ops = [Operator(:∫∫εᵢⱼσᵢⱼdxdy,coefficient...),
       Operator(:∫vᵢgᵢds,coefficient...),
       Operator(:∫σᵢⱼnⱼgᵢvᵢgᵢds,coefficient...),
       Operator(:∫λᵢgᵢds,coefficient...),
       Operator(:∫σ̃̄ᵢⱼnⱼgᵢds,coefficient...)]

k = zeros(2*nₚ,2*nₚ)
f = zeros(2*nₚ)
g = zeros(2*nₗ,2*nₚ)
q = zeros(2*nₗ)

# @btime ops[1]($elements["Ω"],$k)
@btime ops[1]($elements["Ω̃"],$k)
@btime ops[2]($elements["Γᵍ₁"],$k,$f)
@btime ops[2]($elements["Γᵍ₂"],$k,$f)
@btime ops[3]($elements["Γ̃ᵍ₁"],$k,$f)
@btime ops[3]($elements["Γ̃ᵍ₂"],$k,$f)
@btime ops[4]([$elements["Γᵍ₁"];$elements["Γᵍ₂"]],$elements["Γˡ"],$g,$q)

elements["Γᵍ₁"] = ReproducingKernel{type...,:Tri3}(elements["Ω∩Γᵍ₁"],elements["Γᵍ₁"])
elements["Γᵍ₂"] = ReproducingKernel{type...,:Tri3}(elements["Ω∩Γᵍ₂"],elements["Γᵍ₂"])
@btime set∇̃𝝭!($elements["Γᵍ₁"],$elements["Ω∩Γᵍ₁"])
@btime set∇̃𝝭!($elements["Γᵍ₂"],$elements["Ω∩Γᵍ₂"])
@btime set∇̄𝝭!($elements["Γᵍ₁"])
@btime set∇̄𝝭!($elements["Γᵍ₂"])
@btime ops[5]($elements["Γᵍ₁"],$k,$f)
@btime ops[5]($elements["Γᵍ₂"],$k,$f)
