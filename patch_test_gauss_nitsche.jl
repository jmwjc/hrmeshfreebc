
using Revise,ApproxOperator,DataFrames,XLSX

elements, nodes = importmsh("./msh/patchtest.msh")
nₚ = length(nodes[:x])
nₑ = length(elements["Ω"])

# type = (Node,:Quadratic2D,:□,:CubicSpline)
# s = 0.25*ones(nₚ)
# n = 2
type = (Node,:Cubic2D,:□,:CubicSpline)
s = 0.35*ones(nₚ)
n = 3
sp = RegularGrid(nodes[:x],nodes[:y],nodes[:z],n = 3,γ = 5)
elements["Ω"] = ReproducingKernel{type...,:Tri3}(elements["Ω"],sp)
elements["Γᵍ"] = ReproducingKernel{type...,:Seg2}(elements["Γᵍ"],sp)

set𝓖!(elements["Ω"],:TriGI16,:∂1,:∂x,:∂y,:∂z)
set𝓖!(elements["Γᵍ"],:SegGI4,:∂1,:∂x,:∂y,:∂z)

push!(nodes,:s₁=>s,:s₂=>s,:s₃=>s)
prescribe!(elements["Ω"],:b,(x,y,z)->-13.0*n*(n-1)*(1.0+2.0*x+3.0*y)^(n-2))
prescribe!(elements["Γᵍ"],:g,(x,y,z)->(1.0+2.0*x+3.0*y)^n)

coefficient = (:k=>1.0,:α=>1e3)
ops = [Operator(:∫∇v∇udΩ,coefficient...),
       Operator(:∫vbdΩ,coefficient...),
       Operator(:∫∇𝑛vgdΓ,coefficient...),
       Operator(:∫vgdΓ,coefficient...),
       Operator(:H₁,coefficient...)]

k = zeros(nₚ,nₚ)
f = zeros(nₚ)

ops[1](elements["Ω"],k)
ops[2](elements["Ω"],f)
ops[3](elements["Γᵍ"],k,f)
ops[4](elements["Γᵍ"],k,f)

d = k\f

push!(nodes,:d=>d)
set𝓖!(elements["Ω"],:TriGI16,:∂1,:∂x,:∂y,:∂z)
prescribe!(elements["Ω"],:u,(x,y,z)->(1.0+2.0*x+3.0*y)^n)
prescribe!(elements["Ω"],:∂u∂x,(x,y,z)->2.0*n*(1.0+2.0*x+3.0*y)^(n-1))
prescribe!(elements["Ω"],:∂u∂y,(x,y,z)->3.0*n*(1.0+2.0*x+3.0*y)^(n-1))
h1,l2 = ops[5](elements["Ω"])

inte = 100
n̄ₚ = (inte+1)^2
x = zeros(n̄ₚ)
y = zeros(n̄ₚ)
u = zeros(n̄ₚ)
∂u∂x = zeros(n̄ₚ)
∂u∂y = zeros(n̄ₚ)
𝗠 = elements["Ω"][1].𝗠
𝝭 = elements["Ω"][1].𝝭
ap = ReproducingKernel{type...,:Node}([Node(i,nodes) for i in 1:nₚ],Node[],𝗠,𝝭)
for i in 0:inte
    for j in 0:inte
        xᵢ = 1.0/inte*i
        yᵢ = 1.0/inte*j
        x[(inte+1)*j+i+1] = xᵢ
        y[(inte+1)*j+i+1] = yᵢ
        𝒙 = (xᵢ,yᵢ,0.0)
        uᵢ,∂uᵢ∂x,∂uᵢ∂y = get∇𝑢(ap,𝒙,sp)
        u[(inte+1)*j+i+1] = uᵢ
        ∂u∂x[(inte+1)*j+i+1] = ∂uᵢ∂x
        ∂u∂y[(inte+1)*j+i+1] = ∂uᵢ∂y
    end
end

df = DataFrame(x=x,y=y,u=u,∂u∂x=∂u∂x,∂u∂y=∂u∂y)
XLSX.openxlsx("./xlsx/patch_test.xlsx", mode="rw") do xf
    name = "gauss_p="*string(n)*"_nitsche"
    name∉XLSX.sheetnames(xf) ? XLSX.addsheet!(xf,name) : nothing
    XLSX.writetable!(xf[name],df)
end
