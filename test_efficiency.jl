

using ApproxOperator,BenchmarkTools

ndiv = 32
elements, nodes = importmsh("./msh/cantilever_"*string(ndiv)*".msh")
nₚ = length(nodes[:x])
nₑ = length(elements["Ω"])

type = (SNode,:Quadratic2D,:□,:CubicSpline)
s = 2.5*12.0/ndiv*ones(nₚ)
sp = RegularGrid(nodes[:x],nodes[:y],nodes[:z],n = 2,γ = 5)
elements["Ω"] = ReproducingKernel{type...,:Tri3}(elements["Ω"],sp)
elements["Ω̃"] = ReproducingKernel{type...,:Tri3}(elements["Ω"])
elements["Γˡ"] = Element{:Seg2}(elements["Γᵍ"],renumbering=true)
elements["Γ̃ᵍ"] = ReproducingKernel{type...,:Seg2}(elements["Γᵍ"])
elements["Γᵍ"] = ReproducingKernel{type...,:Seg2}(elements["Γᵍ"],sp)
elements["Ω∩Γᵍ"] = elements["Ω"]∩elements["Γᵍ"]
nₗ = getnₚ(elements["Γˡ"])

# set𝓖!(elements["Ω"],:TriGI13,:∂1,:∂x,:∂y,:∂z)
# set𝓖!(elements["Γᵍ"],:SegGI3,:∂1)
# set𝓖!(elements["Γ̃ᵍ"],:SegGI3,:∂1,:∂x,:∂y,:∂z)
set𝓖!(elements["Ω"],:TriRK6,:∂1)
set𝓖!(elements["Ω̃"],:TriGI3,:∂1,:∂x,:∂y,:∂z)
set𝓖!(elements["Γᵍ"],:SegRK3,:∂1,:∂x,:∂y,:∂z,:∂̄x,:∂̄y)
set𝓖!(elements["Γ̃ᵍ"],:SegRK3,:∂1,:∂x,:∂y,:∂z)
elements["Γ̃ᵍ"] = ReproducingKernel{type...,:Tri3}(elements["Ω∩Γᵍ"],elements["Γ̃ᵍ"])

E = 3E6;ν = 0.3;P = 1000;L = 48;D = 12;
I = D^3/12
EI = E*I

prescribe!(elements["Γᵍ"],:g₁,(x,y,z)->-P*y/6/EI*((6*L-3x)*x + (2+ν)*(y^2-D^2/4)))
prescribe!(elements["Γᵍ"],:g₂,(x,y,z)->P/6/EI*(3*ν*y^2*(L-x) + (4+5*ν)*D^2*x/4 + (3*L-x)*x^2))
prescribe!(elements["Γᵍ"],:n₁₁,(x,y,z)->1.0)
prescribe!(elements["Γᵍ"],:n₂₂,(x,y,z)->1.0)
prescribe!(elements["Γ̃ᵍ"],:g₁,(x,y,z)->-P*y/6/EI*((6*L-3x)*x + (2+ν)*(y^2-D^2/4)))
prescribe!(elements["Γ̃ᵍ"],:g₂,(x,y,z)->P/6/EI*(3*ν*y^2*(L-x) + (4+5*ν)*D^2*x/4 + (3*L-x)*x^2))
prescribe!(elements["Γ̃ᵍ"],:n₁₁,(x,y,z)->1.0)
prescribe!(elements["Γ̃ᵍ"],:n₂₂,(x,y,z)->1.0)

push!(nodes,:s₁=>s,:s₂=>s,:s₃=>s)
@btime set𝝭!($elements["Ω"])
@btime set∇̃𝝭!($elements["Ω̃"],$elements["Ω"])
@btime set𝝭!($elements["Γᵍ"])
@btime set∇𝝭!($elements["Γ̃ᵍ"])

coefficient = (:E=>E,:ν=>ν,:α=>1e7*E)
ops = [Operator(:∫∫εᵢⱼσᵢⱼdxdy,coefficient...),
       Operator(:∫vᵢgᵢds,coefficient...),
       Operator(:∫σᵢⱼnⱼgᵢds,coefficient...),
       Operator(:∫λᵢgᵢds,coefficient...),
       Operator(:∫σ̃̄ᵢⱼnⱼgᵢds,coefficient...)]

k = zeros(2*nₚ,2*nₚ)
f = zeros(2*nₚ)
g = zeros(2*nₗ,2*nₚ)
q = zeros(2*nₗ)

# @btime ops[1]($elements["Ω"],$k)
@btime ops[1]($elements["Ω̃"],$k)
@btime ops[2]($elements["Γᵍ"],$k,$f)
@btime ops[3]($elements["Γ̃ᵍ"],$k,$f)
@btime ops[4]($elements["Γᵍ"],$elements["Γˡ"],$g,$q)

elements["Γᵍ"] = ReproducingKernel{type...,:Tri3}(elements["Ω"],elements["Γᵍ"])
@btime set∇̃𝝭!($elements["Γᵍ"],$elements["Ω∩Γᵍ"])
@btime set∇̄𝝭!($elements["Γᵍ"])
@btime ops[5]($elements["Γᵍ"],$k,$f)
