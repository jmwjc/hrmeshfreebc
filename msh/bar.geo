
a = 1.0;
n = 16;

Point(1) = {0.0, 0.0, 0.0, 0.06};
Point(2) = {0.25, 0.0, 0.0,0.04};
Point(3) = {0.5, 0.0, 0.0,0.07};
Point(4) = {0.75, 0.0, 0.0,0.05};
Point(5) = {1.0, 0.0, 0.0,0.06};

Line(1) = {1,2,3,4,5};

// Transfinite Curve{1} = n;
Physical Point("Γ") = {1,5};
Physical Curve("Ω") = {1};

Mesh.Algorithm = 1;
Mesh.MshFileVersion = 2;
Mesh 1;
