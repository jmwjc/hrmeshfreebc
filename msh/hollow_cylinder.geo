/*********************************************************************
 *
 *  hollow_cylinder
 *
 *********************************************************************/

a = 1.0;
b = 2.0;
n = 8;

Point(1) = {0 , 0., 0.};
Point(2) = {a , 0., 0.};
Point(3) = {b , 0., 0.};
Point(4) = {0., b,  0.};
Point(5) = {0., a,  0.};

Line(1)  = {2,3};
Circle(2)  = {5,1,2};
Line(3)  = {4,5};
Circle(4)  = {3,1,4};

Line Loop(5) = { 1, 4, 3, 2};

Plane Surface(1) = {5};

Physical Surface("Ω") = {1};
Physical Curve("Γᵍ₁") = {1};
Physical Curve("Γᵍ₂") = {3};
Physical Curve("Γᵗ₁") = {2};
Physical Curve("Γᵗ₂") = {4};

Transfinite Curve{1,-3} = n+1;
// Transfinite Curve{1,-3} = n+1 Using Progression 1+0.8/n;
Transfinite Curve{2,4} = 2*n+1;
Transfinite Surface{1} Right;

Mesh.Algorithm = 8;
Mesh.MshFileVersion = 2;
Mesh 2;
