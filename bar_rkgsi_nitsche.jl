
using ApproxOperator, DataFrames, XLSX

elements, nodes = importmsh("./msh/bar.msh")
nₚ = length(nodes[:x])
nₑ = length(elements["Ω"])

# type = (:Quadratic1D,:□,:CubicSpline)
# s = 0.25*ones(nₚ)
# n = 2
type = (:Cubic1D,:□,:CubicSpline)
s = 0.35*ones(nₚ)
n = 3

sp = RegularGrid(nodes[:x],nodes[:y],nodes[:z],n = 10,γ = 5)
elements["Ω"] = ReproducingKernel{SNode,type...,:Seg2}(elements["Ω"],sp)
elements["Ω̃"] = ReproducingKernel{SNode,type...,:Seg2}(elements["Ω"])
elements["Γ̃"] = ReproducingKernel{Node,type...,:Poi1}(elements["Γ"])
elements["Γ"] = ReproducingKernel{Node,type...,:Poi1}(elements["Γ"],sp)
push!(nodes,:s₁=>s,:s₂=>s,:s₃=>s)

# set𝓖!(elements["Ω"],:SegRK3,:∂1)
# set𝓖!(elements["Ω̃"],:SegGI2,:∂1,:∂x,:∂y,:∂z)
set𝓖!(elements["Ω"],:SegRK4,:∂1)
set𝓖!(elements["Ω̃"],:SegGI3,:∂1,:∂x,:∂y,:∂z)
set𝓖!(elements["Γ"],:PoiGI1,:∂1)
set𝓖!(elements["Γ̃"],:PoiGI1,:∂1,:∂x,:∂y,:∂z)
elements["Γ̃"] = ReproducingKernel{Node,type...,:Seg2}(elements["Ω"],elements["Γ̃"])

set𝝭!(elements["Ω"])
set∇̃𝝭!(elements["Ω̃"],elements["Ω"])

prescribe!(elements["Ω"],:b,(x,y,z)->-4.0*n*(n-1)*(1.0+2.0*x)^(n-2))
prescribe!(elements["Γ"],:g,(x,y,z)->(1.0+2.0*x)^n)
prescribe!(elements["Γ̃"],:g,(x,y,z)->(1.0+2.0*x)^n)

coefficient = (:k=>1.0,:α=>1e3)
ops = [Operator(:∫∇v∇udΩ,coefficient...),
       Operator(:∫vbdΩ,coefficient...),
       Operator(:∫∇𝑛vgdΓ,coefficient...),
       Operator(:∫vgdΓ,coefficient...),
       Operator(:H₁)]

k = zeros(nₚ,nₚ)
f = zeros(nₚ)

ops[1](elements["Ω̃"],k)
ops[2](elements["Ω"],f)
ops[3](elements["Γ̃"],k,f)
ops[4](elements["Γ"],k,f)
d = k\f

push!(nodes,:d=>d)
set𝓖!(elements["Ω"],:SegGI10,:∂1,:∂x,:∂y,:∂z)
prescribe!(elements["Ω"],:u,(x,y,z)->(1.0+2.0*x)^n)
prescribe!(elements["Ω"],:∂u∂x,(x,y,z)->2.0*n*(1.0+2.0*x)^(n-1))
set∇𝝭!(elements["Ω"])
h1, l2 = ops[5](elements["Ω"])

inte = 100
n̄ₚ = inte+1
x = zeros(n̄ₚ)
Δu = zeros(n̄ₚ)
Δ∂u∂x = zeros(n̄ₚ)
𝗠 = elements["Ω"][1].𝗠
𝝭 = elements["Ω"][1].𝝭
ap = ReproducingKernel{Node,type...,:Node}([Node(i,nodes) for i in 1:nₚ],Node[],𝗠,𝝭)
for i in 0:inte
    xᵢ = 1.0/inte*i
    x[i+1] = xᵢ
    𝒙 = (xᵢ,0.0,0.0)
    uᵢ,∂uᵢ∂x = get∇𝑢(ap,𝒙,sp)
    Δu[i+1] = uᵢ - (1.0+2.0*xᵢ)^n
    Δ∂u∂x[i+1] = ∂uᵢ∂x - 2.0*n*(1.0+2.0*xᵢ)^(n-1)
end

df = DataFrame(x=x,Δu=Δu,Δ∂u∂x=Δ∂u∂x)
XLSX.openxlsx("./xlsx/bar.xlsx", mode="rw") do xf
    name = "rkgsi_p="*string(n)*"_nitsche"
    name∉XLSX.sheetnames(xf) ? XLSX.addsheet!(xf,name) : nothing
    XLSX.writetable!(xf[name],df)
end
