
using Revise,ApproxOperator,DataFrames,XLSX,Beep

# ndiv = 24
elements, nodes = importmsh("./msh/patchtest_3.msh")
# elements, nodes = importmsh("./msh/plate_with_hole_"*string(ndiv)*".msh")
nₚ = length(nodes[:x])
nₑ = length(elements["Ω"])

# type = (SNode,:Quadratic2D,:□,:CubicSpline)
# s = 2.5/15*ones(nₚ)
# n = 1
type = (SNode,:Cubic2D,:□,:CubicSpline)
s = 3.5/15*ones(nₚ)
n = 3
sp = RegularGrid(nodes[:x],nodes[:y],nodes[:z],n = 2,γ = 5)
elements["Ω"] = ReproducingKernel{type...,:Tri3}(elements["Ω"],sp)
elements["Ω̃"] = ReproducingKernel{type...,:Tri3}(elements["Ω"])
elements["Γᵍ"] = ReproducingKernel{type...,:Seg2}(elements["Γᵍ"])
elements["Ω∩Γᵍ"] = elements["Ω"]∩elements["Γᵍ"]

# set𝓖!(elements["Ω"],:TriRK6,:∂1,:∂x,:∂y,:∂z)
# set𝓖!(elements["Ω̃"],:TriGI3,:∂1,:∂x,:∂y,:∂z)
# set𝓖!(elements["Γᵍ"],:SegRK3,:∂1,:∂x,:∂y,:∂z,:∂̄x,:∂̄y)
set𝓖!(elements["Ω"],:TriRK13,:∂1,:∂x,:∂y,:∂z)
set𝓖!(elements["Ω̃"],:TriGI6,:∂1,:∂x,:∂y,:∂z)
set𝓖!(elements["Γᵍ"],:SegRK5,:∂1,:∂x,:∂y,:∂z,:∂̄x,:∂̄y)
elements["Γᵍ"] = ReproducingKernel{type...,:Tri3}(elements["Ω∩Γᵍ"],elements["Γᵍ"])

E = 1.0;ν = 0.3;
Cᵢᵢᵢᵢ = E/(1-ν^2)
Cᵢᵢⱼⱼ = E*ν/(1-ν^2)
Cᵢⱼᵢⱼ = E/2/(1+ν)
u(x,y,z) = (1.0+2.0*x+3.0*y)^n
v(x,y,z) = (4.0+5.0*x+6.0*y)^n
∂u∂x(x,y,z) = 2.0*n*(1.0+2.0*x+3.0*y)^(n-1)
∂u∂y(x,y,z) = 3.0*n*(1.0+2.0*x+3.0*y)^(n-1)
∂v∂x(x,y,z) = 5.0*n*(4.0+5.0*x+6.0*y)^(n-1)
∂v∂y(x,y,z) = 6.0*n*(4.0+5.0*x+6.0*y)^(n-1)
∂²u∂x²(x,y) = 4.0*n*(n-1)*(1.0+2.0*x+3.0*y)^abs(n-2)
∂²u∂x∂y(x,y) = 6.0*n*(n-1)*(1.0+2.0*x+3.0*y)^abs(n-2)
∂²u∂y²(x,y) = 9.0*n*(n-1)*(1.0+2.0*x+3.0*y)^abs(n-2)
∂²v∂x²(x,y) = 25.0*n*(n-1)*(4.0+5.0*x+6.0*y)^abs(n-2)
∂²v∂x∂y(x,y) = 30.0*n*(n-1)*(4.0+5.0*x+6.0*y)^abs(n-2)
∂²v∂y²(x,y) = 36.0*n*(n-1)*(4.0+5.0*x+6.0*y)^abs(n-2)
σ₁₁(x,y) = Cᵢᵢᵢᵢ*∂u∂x(x,y,0) + Cᵢᵢⱼⱼ*∂v∂y(x,y,0)
σ₂₂(x,y) = Cᵢᵢⱼⱼ*∂u∂x(x,y,0) + Cᵢᵢᵢᵢ*∂v∂y(x,y,0)
σ₁₂(x,y) = Cᵢⱼᵢⱼ*(∂u∂y(x,y,0) + ∂v∂x(x,y,0))
∂σ₁₁∂x(x,y) = Cᵢᵢᵢᵢ*∂²u∂x²(x,y) + Cᵢᵢⱼⱼ*∂²v∂x∂y(x,y)
∂σ₂₂∂y(x,y) = Cᵢᵢⱼⱼ*∂²u∂x∂y(x,y) + Cᵢᵢᵢᵢ*∂²v∂y²(x,y)
∂σ₁₂∂x(x,y) = Cᵢⱼᵢⱼ*(∂²u∂x∂y(x,y) + ∂²v∂x²(x,y))
∂σ₁₂∂y(x,y) = Cᵢⱼᵢⱼ*(∂²u∂y²(x,y) + ∂²v∂x∂y(x,y))
b₁(x,y,z) = -∂σ₁₁∂x(x,y) - ∂σ₁₂∂y(x,y)
b₂(x,y,z) = -∂σ₁₂∂x(x,y) - ∂σ₂₂∂y(x,y)

prescribe!(elements["Ω"],:b₁,b₁)
prescribe!(elements["Ω"],:b₂,b₂)
prescribe!(elements["Γᵍ"],:g₁,u)
prescribe!(elements["Γᵍ"],:g₂,v)
prescribe!(elements["Γᵍ"],:n₁₁,(x,y,z)->1.0)
prescribe!(elements["Γᵍ"],:n₂₂,(x,y,z)->1.0)

push!(nodes,:s₁=>s,:s₂=>s,:s₃=>s)
set𝝭!(elements["Ω"])
set∇̃𝝭!(elements["Ω̃"],elements["Ω"])
# set∇̃𝝭!(elements["Γᵍ"],elements["Ω∩Γᵍ"])
set∇̄𝝭!(elements["Γᵍ"])

coefficient = (:E=>E,:ν=>ν)
ops = [Operator(:∫∫εᵢⱼσᵢⱼdxdy,coefficient...),
       Operator(:∫∫vᵢbᵢdxdy,coefficient...),
       Operator(:∫σᵢⱼnⱼgᵢds,coefficient...),
       Operator(:∫σ̄ᵢⱼnⱼgᵢds,coefficient...),
       Operator(:∫σ̃̄ᵢⱼnⱼgᵢds,coefficient...),
       Operator(:Hₑ_PlaneStress,coefficient...)]

k = zeros(2*nₚ,2*nₚ)
f = zeros(2*nₚ)

# ops[1](elements["Ω̃"],k)
# ops[2](elements["Ω"],f)
# ops[3](elements["Γᵍ"][1],k,f)
# ops[4](elements["Γᵍ"][1],k,f)
ops[5](elements["Γᵍ"][1],k,f)

# d = k\f
# d₁ = d[1:2:2*nₚ]
# d₂ = d[2:2:2*nₚ]
# push!(nodes,:d₁=>d₁,:d₂=>d₂)
# set𝓖!(elements["Ω"],:TriGI16,:∂1,:∂x,:∂y,:∂z)
# set∇𝝭!(elements["Ω"])
# prescribe!(elements["Ω"],:u,u)
# prescribe!(elements["Ω"],:v,v)
# prescribe!(elements["Ω"],:∂u∂x,∂u∂x)
# prescribe!(elements["Ω"],:∂u∂y,∂u∂y)
# prescribe!(elements["Ω"],:∂v∂x,∂v∂x)
# prescribe!(elements["Ω"],:∂v∂y,∂v∂y)
# h1,l2 = ops[6](elements["Ω"])
#
# inte = 100
# n̄ₚ = (inte+1)^2
# x = zeros(n̄ₚ)
# y = zeros(n̄ₚ)
# σ₁₁_ = zeros(n̄ₚ)
# σ₂₂_ = zeros(n̄ₚ)
# σ₁₂_ = zeros(n̄ₚ)
# 𝗠 = elements["Ω"][1].𝗠
# 𝝭 = elements["Ω"][1].𝝭
# ap = ReproducingKernel{type...,:Node}([Node(i,nodes) for i in 1:nₚ],Node[],𝗠,𝝭)
# for i in 0:inte
#     for j in 0:inte
#         xᵢ = 1.0/inte*i
#         yᵢ = 1.0/inte*j
#         x[(inte+1)*j+i+1] = xᵢ
#         y[(inte+1)*j+i+1] = yᵢ
#         𝒙 = (xᵢ,yᵢ,0.0)
#         uᵢ,ε₁₁,ε₂₂,ε₁₂ = get𝝐(ap,𝒙,sp)
#         σ₁₁_[(inte+1)*j+i+1] = Cᵢᵢᵢᵢ*ε₁₁ + Cᵢᵢⱼⱼ*ε₂₂
#         σ₂₂_[(inte+1)*j+i+1] = Cᵢᵢⱼⱼ*ε₁₁ + Cᵢᵢᵢᵢ*ε₂₂
#         σ₁₂_[(inte+1)*j+i+1] = Cᵢⱼᵢⱼ*ε₁₂
#     end
# end
#
# df = DataFrame(x=x,y=y,σ₁₁=σ₁₁_,σ₂₂=σ₂₂_,σ₁₂=σ₁₂_)
# XLSX.openxlsx("./xlsx/patch_test_planestress.xlsx", mode="rw") do xf
#     name = "hrrk_p="*string(n)
#     name∉XLSX.sheetnames(xf) ? XLSX.addsheet!(xf,name) : nothing
#     XLSX.writetable!(xf[name],df)
# end
#
# beep()
